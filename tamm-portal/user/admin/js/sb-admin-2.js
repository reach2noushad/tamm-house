(function($) {
  "use strict"; // Start of use strict
  $("#spinner").show();
  $.get(
    "http://115.114.36.146:3124/logs", function (data) {
      console.log(data);
      if(data && data.houseRequested) {
        var buyReqContainer = $("#dataTableBuyRequest tbody");
        data.houseRequested.forEach((d, i) => {
          var content = `<tr>
            <td>${i+1}</td>
            <td>${d.name}</td>
            <td>${d.requester}</td>
            <td>${d.houseId}</td>
            <td>${d.date}</td>
          </tr>`
          buyReqContainer.append(content);
        });
      }
      if(data && data.muncipalityApproved) {
        var approvedContainer = $("#dataTableApprove tbody");
        data.muncipalityApproved.forEach((d, i) => {
          var content = `<tr>
            <td>${i+1}</td>
            <td>${d.name}</td>
            <td>${d.newOwner}</td>
            <td>${d.houseId}</td>
            <td>${d.approvalAuthority}</td>
            <td>${d.date}</td>
          </tr>`
          approvedContainer.append(content);
        });
        
      }

      // if(data && data.houseSell) {
      //   sellerRequestContainer = $("#dataTableBuyRequest tbody");
      //   data.houseSell.forEach(d, i => {
      //     var content = `<tr>
      //       <td>${i+1}</td>
      //       <td>${d.owner}</td>
      //       <td>${d.houseId}</td>
      //     </tr>`
      //     buyReqContainer.append(content);
      //   });
      // }
      $("#spinner").hide();
    })
  
  // Toggle the side navigation
  $("#sidebarToggle, #sidebarToggleTop").on('click', function(e) {
    $("body").toggleClass("sidebar-toggled");
    $(".sidebar").toggleClass("toggled");
    if ($(".sidebar").hasClass("toggled")) {
      $('.sidebar .collapse').collapse('hide');
    };
  });

  // Close any open menu accordions when window is resized below 768px
  $(window).resize(function() {
    if ($(window).width() < 768) {
      $('.sidebar .collapse').collapse('hide');
    };
  });

  // Prevent the content wrapper from scrolling when the fixed side navigation hovered over
  $('body.fixed-nav .sidebar').on('mousewheel DOMMouseScroll wheel', function(e) {
    if ($(window).width() > 768) {
      var e0 = e.originalEvent,
        delta = e0.wheelDelta || -e0.detail;
      this.scrollTop += (delta < 0 ? 1 : -1) * 30;
      e.preventDefault();
    }
  });

  // Scroll to top button appear
  $(document).on('scroll', function() {
    var scrollDistance = $(this).scrollTop();
    if (scrollDistance > 100) {
      $('.scroll-to-top').fadeIn();
    } else {
      $('.scroll-to-top').fadeOut();
    }
  });

  // Smooth scrolling using jQuery easing
  $(document).on('click', 'a.scroll-to-top', function(e) {
    var $anchor = $(this);
    $('html, body').stop().animate({
      scrollTop: ($($anchor.attr('href')).offset().top)
    }, 1000, 'easeInOutExpo');
    e.preventDefault();
  });

})(jQuery); // End of use 


