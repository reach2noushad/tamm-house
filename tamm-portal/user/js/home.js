let selectedHouse;
var houses;
var address = '0xb1f75fecd5b982aaa0bd6cf83a3371ca7ab21522';
function buyHome() {
	$("#spinner").show();
	var fname = document.getElementsByName("fname")[0].value;
	var lname = document.getElementsByName("lname")[0].value;
	var uname = document.getElementsByName("email")[0].value;
	var pwd = document.getElementsByName("pass")[0].value;
	$.post(
		"http://115.114.36.146:3124/home/buy",
		{ houseId: parseInt(selectedHouse, 10), address },
		function (data) {
			console.log(data);
			$.post("http://115.114.36.146:3124/home" , { houseId: parseInt(selectedHouse, 10)}, function (response) {
				console.log(response);
				document.getElementById("status" + selectedHouse).classList.remove("hidden");
				$("#spinner").hide();
				document.getElementById("status" + selectedHouse).innerText = "On Hold";
			}
			);
		}
	);
}

function setHouseId(id){
	var houseDetails = houses.filter(h => h.houseId == id);
	console.log(houseDetails);
	selectedHouse = id;
	if(houseDetails[0].message !== "forSell"){
		event.target.parentElement.href="";
		event.preventDefault();
		window.alert("The home you are trying to purchase, is already being sold/ part of another transaction")
	}
	else {
		event.target.parentElement.href="#portfolioModal";
	}
}

$(function () {
	var promises = [];
	var def = $.Deferred();
	$("#spinner").show();
	$.get(
		"http://115.114.36.146:3124/listHouse",
		function (data) {
			var homes = $("#portfolios");
			if (data && data.length > 0) {
				houses = data;
				data.forEach((item, index) => {
					var content = `<div class="col-md-4 col-sm-6 portfolio-item">
				<a class="portfolio-link" data-toggle="modal" href="#portfolioModal" onclick="setHouseId(${item.houseId}, event)">
				  <div class="portfolio-hover">
					<div class="portfolio-hover-content">
					  <i class="fas fa-plus fa-3x"></i>
					</div>
				  </div>
				  <img class="img-fluid" src="img/home/home1.jpg" alt="" style ="width:350px; height:225px;">
				</a>
				<div class="portfolio-caption">
					<p id="status${item.houseId}" class="text-muted">${item.message === "forSell" ? "For Sale" : (item.message === "sold" ? "Sold" : (item.message === "requested" ? "On Hold" :"NA"))}</p>
					<h4>Home ${item.houseId}</h4>
					<p class="text-muted" style="font-size:12px;">Owned by : ${item.ownerName}</p>
					<div id="requestedBy">
						${item.requesterName ? `<p class="text-muted" style="font-size:12px;">Buy request from : ${item.requesterName}</p>`: ``}
					</div>
				</div>
				</div>`;
				homes.append(content);
					if (index === data.length - 1) return
				});
			}
			$("#spinner").hide();
		}
	);
})