var housingRequests;

var address = '0x4f82239a07f5dc98a4184835edde94eaa003be15';
function approve(houseId, owner, requester, index) {
  $("#spinner").show();
  $.post(
    "http://115.114.36.146:3124/home/processRequest", {houseId: houseId, address: address}, function (data) {
        console.log(data);
        $("#spinner").hide();
        document.getElementById("action"+index).innerText="Approved"
    })
}

function reject (houseId, owner, requester, index) {
  $("#spinner").show();
  $.post(
    "http://115.114.36.146:3124/home/processRequest", {houseId: houseId, address: address}, function (data) {
        console.log(data);
        $("#spinner").hide();
        document.getElementById("action"+index).innerText="Approved"
    })
}

(function ($) {
  "use strict"; // Start of use strict
  $("#spinner").show();
  $.get(
    "http://115.114.36.146:3124/listMuncipalityHouseIds",
    function (data) {
      var container = $("#collapseCardExample")
      housingRequests = data;
      console.log(housingRequests);
      if(data && data.length > 0) {
        data.forEach(function (d,i){
          if(d.message) {
            var content = `<div class="card-body">
            <div class="row">
              <div class="col-7" id="message${i}">
                ${d.requesterName} has requested to buy Home ${d.houseId} from ${d.ownerName}
              </div>
              <div class="col-5" id="action${i}">
              ${d.message == "requested" ?
                `<a href="javascript:void(0)" onclick="approve(${d.houseId}, ${d.owner}, ${d.requester.toString()}, ${i})" style="margin-left:10px;" class="btn btn-success btn-icon-split float-right">
                  <span class="icon text-white-50">
                    <i class="fas fa-check"></i>
                  </span>
                  <span class="text">Approve</span>
                </a>
                <a href="javascript:void(0)" onclick="reject(${d.houseId}, ${d.owner}, ${d.requester.toString()}, ${i})" class="btn btn-warning btn-icon-split float-right">
                  <span class="icon text-white-50">
                    <i class="fas fa-check"></i>
                  </span>
                  <span class="text">Reject</span>
                </a>`: `Approved`}
              </div>
            </div>
          </div>`;
          container.append(content);
          }
        })
      }
      $("#spinner").hide();
    }
  );
// Toggle the side navigation
$("#sidebarToggle, #sidebarToggleTop").on('click', function (e) {
  $("body").toggleClass("sidebar-toggled");
  $(".sidebar").toggleClass("toggled");
  if ($(".sidebar").hasClass("toggled")) {
    $('.sidebar .collapse').collapse('hide');
  };
});

// Close any open menu accordions when window is resized below 768px
$(window).resize(function () {
  if ($(window).width() < 768) {
    $('.sidebar .collapse').collapse('hide');
  };
});

// Prevent the content wrapper from scrolling when the fixed side navigation hovered over
$('body.fixed-nav .sidebar').on('mousewheel DOMMouseScroll wheel', function (e) {
  if ($(window).width() > 768) {
    var e0 = e.originalEvent,
      delta = e0.wheelDelta || -e0.detail;
    this.scrollTop += (delta < 0 ? 1 : -1) * 30;
    e.preventDefault();
  }
});

// Scroll to top button appear
$(document).on('scroll', function () {
  var scrollDistance = $(this).scrollTop();
  if (scrollDistance > 100) {
    $('.scroll-to-top').fadeIn();
  } else {
    $('.scroll-to-top').fadeOut();
  }
});

// Smooth scrolling using jQuery easing
$(document).on('click', 'a.scroll-to-top', function (e) {
  var $anchor = $(this);
  $('html, body').stop().animate({
    scrollTop: ($($anchor.attr('href')).offset().top)
  }, 1000, 'easeInOutExpo');
  e.preventDefault();
});

}) (jQuery); // End of use strict
