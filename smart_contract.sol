pragma solidity ^0.4.24;



contract TAMM {
    
    uint[] public houseIds;
    uint[] public muncipalityHouseIds;
    
    event houseSell (address owner, uint id, uint time);
    event houseRequested (address requester, uint id, uint time);
    event sellerApproved (uint id, uint time);
    event muncipalityApproved (address newOwner, uint id,uint time);
 
    
    struct House {
        
          uint id;
          address owner;
          address requester;
          bool sell;
          string status; //forSell , requested,  forMuncipality , sold
          
        }
        
    struct User {
        
        address userPublicAddress;
        string name;
        string userType; // seller, buyer , muncipality official
        
        }
        
    mapping (uint=>House) public house;
    mapping (address=>User) public user;
    
    
   function sellHouse(address _presentOwner, uint _id) {
       
       house[_id].id = _id;
       house[_id].owner = _presentOwner;
       house[_id].sell = true;
       house[_id].status = "forSell";
       houseIds.push(_id);
       
       houseSell(_presentOwner,_id,now);
       
   }
   
   function requestHouseForBuying(address _requester, uint _id) public {
       
       house[_id].requester = _requester;
       house[_id].sell = false;
       house[_id].status = "requested";
       muncipalityHouseIds.push(_id);
       
       houseRequested(_requester,_id,now);
       
   }
    
    function approveRequester(uint _id) public {
       
      house[_id].status = "forMuncipality";
      
      sellerApproved(_id,now);
     
     }
   
   function getHouse(uint _id) public constant returns(uint,address,address,bool,string){
       return(house[_id].id,house[_id].owner,house[_id].requester,house[_id].sell,house[_id].status);
   }
   
   function approveHouseSell(uint _id, uint index) {
       
        house[_id].owner =  house[_id].requester;
        house[_id].requester = 0x00;
        house[_id].status = "sold";
        
        delete muncipalityHouseIds[index];
        
        muncipalityApproved(house[_id].owner, _id,now);
       
   }
   
   function getAllHouseIds() constant returns(uint[]) {
       return houseIds;
   }
   
     function getMuncipalityHouseIds() constant returns(uint[]) {
       return muncipalityHouseIds;
   }
   
   
   //user functions
   
   function addUSer(address _user, string _name, string _userType) public {
       
       user[_user].userPublicAddress = _user;
       user[_user].name = _name;
       user[_user].userType = _userType ;
       
   
   }
   
    
    
    
    
}
