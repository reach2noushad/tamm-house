const ethers = require('ethers');
const keythereum = require("keythereum");
const os = require('os');

let provider = ethers.providers.getDefaultProvider('rinkeby');
let abi = [{"constant":false,"inputs":[{"name":"_user","type":"address"},{"name":"_name","type":"string"},{"name":"_userType","type":"string"}],"name":"addUSer","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"_id","type":"uint256"},{"name":"index","type":"uint256"}],"name":"approveHouseSell","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"_id","type":"uint256"}],"name":"approveRequester","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"_requester","type":"address"},{"name":"_id","type":"uint256"}],"name":"requestHouseForBuying","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"_presentOwner","type":"address"},{"name":"_id","type":"uint256"}],"name":"sellHouse","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"anonymous":false,"inputs":[{"indexed":false,"name":"owner","type":"address"},{"indexed":false,"name":"id","type":"uint256"},{"indexed":false,"name":"time","type":"uint256"}],"name":"houseSell","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"name":"requester","type":"address"},{"indexed":false,"name":"id","type":"uint256"},{"indexed":false,"name":"time","type":"uint256"}],"name":"houseRequested","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"name":"id","type":"uint256"},{"indexed":false,"name":"time","type":"uint256"}],"name":"sellerApproved","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"name":"newOwner","type":"address"},{"indexed":false,"name":"id","type":"uint256"},{"indexed":false,"name":"time","type":"uint256"}],"name":"muncipalityApproved","type":"event"},{"constant":true,"inputs":[],"name":"getAllHouseIds","outputs":[{"name":"","type":"uint256[]"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"_id","type":"uint256"}],"name":"getHouse","outputs":[{"name":"","type":"uint256"},{"name":"","type":"address"},{"name":"","type":"address"},{"name":"","type":"bool"},{"name":"","type":"string"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"getMuncipalityHouseIds","outputs":[{"name":"","type":"uint256[]"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"","type":"uint256"}],"name":"house","outputs":[{"name":"id","type":"uint256"},{"name":"owner","type":"address"},{"name":"requester","type":"address"},{"name":"sell","type":"bool"},{"name":"status","type":"string"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"","type":"uint256"}],"name":"houseIds","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"","type":"uint256"}],"name":"muncipalityHouseIds","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"","type":"address"}],"name":"user","outputs":[{"name":"userPublicAddress","type":"address"},{"name":"name","type":"string"},{"name":"userType","type":"string"}],"payable":false,"stateMutability":"view","type":"function"}];
let contractAddress = "0xF9853660FE1a6AB4AA44cF7937d746b1691f70D8";

let datadir = os.homedir();



module.exports = {

    getPrivateKey : async function(publicAddress){

        let address = publicAddress.slice(2);
        let keyObject = keythereum.importFromFile(address, datadir);
        let privateKey = keythereum.recover("password", keyObject);
        return privateKey;

    },

    sellHouse : async function(address, houseId, privateKey) { 

        try {
            

        var wallet = new ethers.Wallet(privateKey,provider);
        var contract = new ethers.Contract(contractAddress,abi,wallet);

        let transaction = await contract.sellHouse(address,houseId);
        await provider.waitForTransaction(transaction.hash);
        return true;

        } catch(e) {

            console.log(e);
            return error;
        }
    },

    buyHouse : async function(address, houseId, privateKey) { 

        try {

            var wallet = new ethers.Wallet(privateKey,provider);
            var contract = new ethers.Contract(contractAddress,abi,wallet);

            //getlistOfMuncipality and ensure house is not already rquested by any buyer
            const muncipalityHouseId = await contract.getMuncipalityHouseIds();
            const muncipalityHouseIds = muncipalityHouseId.map(house => house.toNumber());

            if(muncipalityHouseIds.indexOf(houseId) == -1){

                console.log("doesnot exist, proceed inserting");

                let transaction = await contract.requestHouseForBuying(address,houseId);
                await provider.waitForTransaction(transaction.hash);
                return true;



            }else{
                console.log("exists");
                throw new Error("House already present in Municipality Pending List");
            }


        } catch (e) {

            console.log(e);
            return error;

        }

    },

    sellerApprove : async function(houseId, privateKey) { 

        try {

            var wallet = new ethers.Wallet(privateKey,provider);
            var contract = new ethers.Contract(contractAddress,abi,wallet);

            let transaction = await contract.approveRequester(houseId);
            await provider.waitForTransaction(transaction.hash);
            return true;

        } catch (e) {

            console.log(e);
            return error;

        }

    },

    approveHouseSell : async function(houseId, privateKey) { 

        try {

            var wallet = new ethers.Wallet(privateKey,provider);
            var contract = new ethers.Contract(contractAddress,abi,wallet);

            console.log(+houseId);
            console.log(typeof(houseId));

            //get index of house ID in muncipality list
            muncipalityIdBN = await contract.getMuncipalityHouseIds();
            const muncipalityId = muncipalityIdBN.map(id => id.toNumber());
            console.log(muncipalityId);
            var index = muncipalityId.indexOf(parseInt(houseId));

            console.log(index);
   
            let transaction = await contract.approveHouseSell(houseId,index);
            await provider.waitForTransaction(transaction.hash);
            return true;

        } catch (e) {

            console.log(e);
            return error;

        }

    },

    getHouseIds : async function() { 

        try {

            var contract = new ethers.Contract(contractAddress,abi,provider);

            return await contract.getAllHouseIds();

        } catch (e) {

            console.log(e);
            return error;

        }

    },

    getMuncipalityHouseIds : async function() { 

        try {

            var contract = new ethers.Contract(contractAddress,abi,provider);

            return await contract.getMuncipalityHouseIds();

        } catch (e) {

            console.log(e);
            return error;

        }

    },

    getHouse : async function(houseId) { 

        try {

            var contract = new ethers.Contract(contractAddress,abi,provider);

            return await contract.getHouse(houseId);

        } catch (e) {

            console.log(e);
            return error;

        }

    },

    addUserDetails : async function(address,name,userType, privateKey) { 

        try {

            var wallet = new ethers.Wallet(privateKey,provider);
            var contract = new ethers.Contract(contractAddress,abi,wallet);

            let transaction = await contract.addUSer(address,name,userType);
            await provider.waitForTransaction(transaction.hash);
            return true;

        } catch (e) {

            console.log(e);
            return error;

        }

    },

    getUserDetails : async function(address) { 

        try {

            var contract = new ethers.Contract(contractAddress,abi,provider);

            return await contract.user(address);

        } catch (e) {

            console.log(e);
            return error;

        }

    }



}


