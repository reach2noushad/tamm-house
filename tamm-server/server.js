var restify = require('restify');
const transaction = require('./transactions');
const helper = require('./helper');
const events = require('./events');

const corsMiddleware =require("restify-cors-middleware");  


const server = restify.createServer({
  name: 'myapp',
  version: '1.0.0'
});

server.use(restify.plugins.acceptParser(server.acceptable));
server.use(restify.plugins.queryParser());
server.use(restify.plugins.bodyParser());

server.get('/services', function (req, res, next) {
  res.send(req.params);
  return next();
});

const cors = corsMiddleware({  
  origins: ["*"],
  allowHeaders: ["Authorization"],
  exposeHeaders: ["Authorization"]
});

server.pre(cors.preflight);  
server.use(cors.actual);  

server.get('/listHouse', async function (req, res, next) {

  const houses =  await transaction.getHouseIds();

  const houseIds = houses.map(house => house.toNumber());
  console.log(helper.names);
  let obj = [];

  for (const i in houseIds) {

    let secondObj = {};


    const result = await transaction.getHouse(houseIds[i]);

    secondObj.houseId = result[0].toNumber();
    secondObj.owner = result[1];
    secondObj.requester = result[2];
    secondObj.status = result[3];
    secondObj.message = result[4];
    secondObj.ownerName = helper.names[result[1].toLowerCase()];
    secondObj.requesterName = helper.names[result[2].toLowerCase()];
    obj.push(secondObj);

  }

 
  res.send(obj);


});

server.get('/listMuncipalityHouseIds', async function (req, res, next) {

  const houses =  await transaction.getMuncipalityHouseIds();

  console.log("here",houses);

  const houseIds = houses.map(house => house.toNumber());
  console.log(houseIds);
  let arr = [];

  for (const i in houseIds) {

  

    let secondObj = {};


    const result = await transaction.getHouse(houseIds[i]);

    console.log(result);

    secondObj.houseId = result[0].toNumber();
    secondObj.owner = result[1];
    secondObj.requester = result[2];
    secondObj.status = result[3];
    secondObj.message = result[4];
    secondObj.ownerName = helper.names[result[1].toLowerCase()];
    secondObj.requesterName = helper.names[result[2].toLowerCase()];
    arr.push(secondObj);

  }

  res.send(arr);

});


server.post('/home/sell', async function (req, res, next) {

  //api to keep house for selling

 
  try {
    

  var address = req.body.address;
  var houseId = req.body.houseId;

 
  const privateKey = await transaction.getPrivateKey(address);

  const result = await transaction.sellHouse(address,houseId,privateKey);

  res.send(result);

  } catch(e) {

    res.send(e);

  }


})

server.post('/home', async function (req, res, next) {

  //api to get house details giving house Id

 
  try {
    

    var houseId = req.body.houseId;

    let secondObj = {};

    const result = await transaction.getHouse(houseId);

    secondObj.houseId = result[0].toNumber();
    secondObj.owner = result[1];
    secondObj.requester = result[2];
    secondObj.status = result[3];
    secondObj.message = result[4];
    secondObj.ownerName = helper.names[result[1].toLowerCase()];
    secondObj.requesterName = helper.names[result[2].toLowerCase()];

    res.send(secondObj);

  } catch(e) {

    res.send(e);

  }


})



server.post('/home/buy', async function (req, res, next) {

  //api to buy house

  console.log("calling house buying api");

  try {
    

  let address = req.body.address;
  let houseId = req.body.houseId;

  const privateKey = await transaction.getPrivateKey(address);

  const result = await transaction.buyHouse(address,houseId,privateKey);

  res.send(result);

  } catch(e) {

    res.send(e);

 }


})


server.post('/home/approveSeller', async function (req, res, next) {

  //api to buy house

  console.log("calling house approval by seller api");

  try {
    

  let address = req.body.address;
  let houseId = req.body.houseId;

  const privateKey = await transaction.getPrivateKey(address);

  const result = await transaction.sellerApprove(houseId,privateKey);

  res.send(result);

  } catch(e) {

    res.send(e);

    }


})

server.post('/home/processRequest', async function (req, res, next) {

  try {

      //approving house sell by Muncipality
      let address = req.body.address;
      let houseId = req.body.houseId;

      console.log("munciplaity approving",address,houseId);

      const privateKey = await transaction.getPrivateKey(address);

      const result = await transaction.approveHouseSell(houseId,privateKey);
      res.send(result);


  } catch(e) {

    res.send(e);

  }

})

server.post('/addUser', async function (req, res, next) {

  try {

      //adding user details like name
      let address = req.body.address;
      let name = req.body.name;
      let userType = req.body.userType;

      const privateKey = await transaction.getPrivateKey(address);

      const result = await transaction.addUserDetails(address,name,userType, privateKey);
      res.send(result);


  } catch(e) {

    res.send(e);

  }

})


server.post('/user', async function (req, res, next) {

  try {

      //getting user by address
      let address = req.body.address;
 
      const result = await transaction.getUserDetails(address);
      res.send(result);


  } catch(e) {

    res.send(e);

  }

})

server.get('/logs', async function (req, res, next) {

  try {

      const eventsList = ["sellHouse","buyerRequest","sellerApprove","muncipalityApprove"];

      let obj = {};

      for (const i in eventsList) {

        let eventType = eventsList[i];
        var eventTopic  = helper.events[eventType].eventTopic;
        var eventName  = helper.events[eventType].eventName;
        console.log(eventType,eventTopic,eventName);
        const result = await events.getAllEventLogs(eventTopic,eventName);

        obj[eventName] = result;

      }

      res.send(obj);


  } catch(e) {

    res.send(e);

  }

})

server.listen(3124, function () {
  console.log('%s listening at %s', server.name, server.url);
});