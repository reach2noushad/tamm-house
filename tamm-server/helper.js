
exports.events = {
    sellHouse : {
        eventName : "houseSell",
        eventTopic : "0x95d12e6c12de8ddb4798b984500051ff80fe89a09a0ac500663eed2ac313c854"
    },
    buyerRequest : {
        eventName : "houseRequested",
        eventTopic : "0xa87c10938af3cebf2392cf5ff34b801888c558646738026514983a0552e21d50"
    },
    sellerApprove : {
        eventName : "sellerApproved",
        eventTopic : "0xa11600b6962fe30382ad42f0a8a1671cd2ef378bd873a9e4ba3e608d5c9221b2"
    },
    muncipalityApprove : {
        eventName : "muncipalityApproved",
        eventTopic : "0xa129472f6e134c73cfe82fd64314c0d93d41c39b117acb2fa8674809d75658a6"
    }
}

exports.names = {

    "0xb1f75fecd5b982aaa0bd6cf83a3371ca7ab21522" :"Hameed", //buyer
    "0xe1ae64b45fc48b27c3ad71c5fcd551aa29df7827" :"Ahamed", //owner
    "0x4f82239a07f5dc98a4184835edde94eaa003be15" :"Housing Authority",
    "0xf9a5e5feac9697d2f81786c38ccdf882dcbdf813" :"Admin",

}