const ethers = require('ethers');
const provider = ethers.providers.getDefaultProvider('rinkeby');
let contractAddress = "0xF9853660FE1a6AB4AA44cF7937d746b1691f70D8";

const Interface = ethers.Interface;
const abi = [{"constant":false,"inputs":[{"name":"_user","type":"address"},{"name":"_name","type":"string"},{"name":"_userType","type":"string"}],"name":"addUSer","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"_id","type":"uint256"},{"name":"index","type":"uint256"}],"name":"approveHouseSell","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"_id","type":"uint256"}],"name":"approveRequester","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"_requester","type":"address"},{"name":"_id","type":"uint256"}],"name":"requestHouseForBuying","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"_presentOwner","type":"address"},{"name":"_id","type":"uint256"}],"name":"sellHouse","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"anonymous":false,"inputs":[{"indexed":false,"name":"owner","type":"address"},{"indexed":false,"name":"id","type":"uint256"},{"indexed":false,"name":"time","type":"uint256"}],"name":"houseSell","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"name":"requester","type":"address"},{"indexed":false,"name":"id","type":"uint256"},{"indexed":false,"name":"time","type":"uint256"}],"name":"houseRequested","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"name":"id","type":"uint256"},{"indexed":false,"name":"time","type":"uint256"}],"name":"sellerApproved","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"name":"newOwner","type":"address"},{"indexed":false,"name":"id","type":"uint256"},{"indexed":false,"name":"time","type":"uint256"}],"name":"muncipalityApproved","type":"event"},{"constant":true,"inputs":[],"name":"getAllHouseIds","outputs":[{"name":"","type":"uint256[]"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"_id","type":"uint256"}],"name":"getHouse","outputs":[{"name":"","type":"uint256"},{"name":"","type":"address"},{"name":"","type":"address"},{"name":"","type":"bool"},{"name":"","type":"string"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"getMuncipalityHouseIds","outputs":[{"name":"","type":"uint256[]"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"","type":"uint256"}],"name":"house","outputs":[{"name":"id","type":"uint256"},{"name":"owner","type":"address"},{"name":"requester","type":"address"},{"name":"sell","type":"bool"},{"name":"status","type":"string"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"","type":"uint256"}],"name":"houseIds","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"","type":"uint256"}],"name":"muncipalityHouseIds","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"","type":"address"}],"name":"user","outputs":[{"name":"userPublicAddress","type":"address"},{"name":"name","type":"string"},{"name":"userType","type":"string"}],"payable":false,"stateMutability":"view","type":"function"}];
const iface = new Interface(abi);
const eventInfo = iface.events;

module.exports = {

    getAllEventLogs: async (eventTopic,eventName) => {

          var filter = {
            fromBlock: 3971238,
            address: contractAddress,
            topics: [ eventTopic ]
         }

         var eventLogs = await provider.getLogs(filter);

        let a = [];

         for (const i in eventLogs) {

            let newEventInfo = eventInfo[eventName];

            var eventParsed = newEventInfo.parse(eventLogs[i].topics, eventLogs[i].data);
            console.log(eventParsed);

            let b = {};

            if(eventName == "houseSell") {

                b.owner = eventParsed.owner
                console.log()
                b.name = "Ahamed" //seller
               

            } else if (eventName == "houseRequested") {

                b.requester = eventParsed.requester;
                b.name = "Hameed" //buyer

            } else if (eventName == "muncipalityApproved") {
                b.newOwner = eventParsed.newOwner;
                b.name = "Hameed";
                b.approvalAuthority = "Housing Authority";
            }

            b.houseId = eventParsed.id.toNumber();
            let date = new Date(eventParsed.time.toNumber()*1000).toLocaleDateString();
            let time = new Date(eventParsed.time.toNumber()*1000).toLocaleTimeString();
            b.date = date + " " + time;
            

            a.push(b);


         }

         return a;
   
    }
}


